<? 
session_start();
include('config.php');
include('db.php');
include('core.php');
// TODO:
// 1. роутинг через .gitignore
// 2. редирект на страницу со slug = id системы

if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_REQUEST['system_id'])) {
	$system = System::get(intval($_REQUEST['system_id']));
	if(!empty($system)) {
		$system_id = $_SESSION['system'] = $_REQUEST['system_id'];
		$system_name = $system['title'];		
	}
}
else if(!empty($_SESSION['system'])) {
	$system = System::get(intval($_SESSION['system']));
	$system_id = $_SESSION['system'];
	$system_name = $system['title'];		
	
}
else {
	$system_id = $_SESSION['system'] = isset($_SERVER['HTTP_EVE_SOLARSYSTEMID'])?$_SERVER['HTTP_EVE_SOLARSYSTEMID']:0;
	$system_name = isset($_SERVER['HTTP_EVE_SOLARSYSTEMNAME'])?$_SERVER['HTTP_EVE_SOLARSYSTEMNAME']:'';		
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<title>WH-Scanner</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="icon" href="/favicon.png" type="image/x-icon" />
	<link href="/css/style.css" rel="stylesheet" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body onload="CCPEVE.requestTrust('http://<?=$_SERVER['HTTP_HOST']; ?>');">
<div class="wrapper">
	<header class="header">
		<!--h1>Wormhole<br>Scanner</h1-->
		<a href="/"></a>
		<div class="input-wrap">
			<form action="" method="POST">
				<p>
					<span>System:</span>
					<input type="text" name="system_id" />
					<input type="submit" value="Go" />
				</p>
			</form>			
		</div>
	</header><!-- .header-->

	<div class="content">
	<?
	// проверка на доверенность сайту
	// если зашли не из Евы
	if(empty($_SERVER['HTTP_EVE_TRUSTED']) || strtolower($_SERVER['HTTP_EVE_TRUSTED']) != 'yes') {
		// если задана система
		if($system_id) {
			$system = new System($system_id , $system_name);
			display('layouts/web.php', array('system' => $system));
		} // если не задана
		else {
			display('layouts/no_system.php');
		}
	} // зашли из Евы через IGB
	else {
		// получаем инфу 
		$character = new Character($_SERVER['HTTP_EVE_CHARID'] ,$_SERVER['HTTP_EVE_CHARNAME']);
		$corporation = new Corporation($_SERVER['HTTP_EVE_CORPID'] ,$_SERVER['HTTP_EVE_CORPNAME']);
		$system = new System($system_id , $system_name);
		$system_active = new System($_SERVER['HTTP_EVE_SOLARSYSTEMID'] ,$_SERVER['HTTP_EVE_SOLARSYSTEMNAME']);
		$ship = isset($_SERVER['HTTP_EVE_SHIPTYPENAME'])?$_SERVER['HTTP_EVE_SHIPTYPENAME']:'unknown';
				
		// добавляем визит только если в эту сессию еще не добавляли с такой системой
		$lastsystem = isset($_SESSION['lastsystem'])?$_SESSION['lastsystem']:'';
		$lastship = isset($_SESSION['lastship'])?$_SESSION['lastship']:'';		
		if($lastsystem != $system->id || $lastship != $ship) {
			$visit = new Visit($character, $corporation, $system_active, $ship);
		}		
		// сохраняем параметры последнего визита 
		$_SESSION['lastsystem'] = $system->id;
		$_SESSION['lastship'] = $ship;

        // выводим шаблон
        display('layouts/igb.php', array(
        'system' => $system,
        'character' => $character,
        'corporation' => $corporation,
        'ship' => $ship
        ));
        }
        ?>
        </div><!-- .content -->
        </div><!-- .wrapper -->
        </body>

        <footer class="footer">
    <p>© 2014 - 2017 WH SCANNER</p>
</footer><!-- .footer -->
</html>