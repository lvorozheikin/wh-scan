<pre>
<?
set_time_limit(0);

include('../config.php');
include('../db.php');
include('../core.php');

echo 'Migrating `Character`...'.PHP_EOL;
Character::migrate();

echo 'Migrating `Corporation`...'.PHP_EOL;
Corporation::migrate();

echo 'Migrating `System`...'.PHP_EOL;
System::migrate();

echo 'Migrating `Visit`...'.PHP_EOL;
Visit::migrate();

echo 'Migrating `Anomaly`...'.PHP_EOL;
Anomaly::migrate();

echo 'DONE!'.PHP_EOL;
?>
</pre>
