<?
set_time_limit(0);

include('../config.php');
include('../db.php');
include('../core.php');
include('../libs/simple_html_dom.php');
$types = array(
	1 => 'Anomaly',
	2 => 'Relic',
	3 => 'Data',
	4 => 'Gas',
	5 => 'Ore',
);

if(isset($_REQUEST['system_id'])) {
	parseClass($_REQUEST['system_id'], $_REQUEST['class_id']);
}
else {
// получаем список ссылок на аномальки
	$sql = "SELECT * FROM `systems` WHERE `class_id` IS NOT NULL GROUP BY `class_id`";
	if ($stmt = $db->prepare($sql)) {
		$stmt->execute();
		// бегаем по классам систем
		while ($s = $stmt->fetch()) {
			parseClass($s['system_id'], $s['class_id']);
			usleep(300);
		}
	}
	else echo "Error!";
}

function parseClass($system_id, $class_id) {
	global $types;
	$url = "http://www.ellatha.com/eve/WormholeSystemview.asp?key=".$system_id;
	echo "$url<br><br>";
	$html = file_get_contents($url);
	$pattern = '~\<b\>Cosmic Anomalies\<\/b\>(.*?)\<b\>Relic Sites - Magnetometric\<\/b\>(.*?)\<b\>Data Sites - Radar\<\/b\>(.*?)\<b\>Gas Sites - Ladar\<\/b\>(.*?)\<b\>Ore Sites - Gravimetric\<\/b\>(.*?)\<\/td\>~is';
	preg_match($pattern, $html, $anomaly_groups);
	unset($anomaly_groups[0]);
	// бегаем по типам аномалек
	foreach($anomaly_groups as $type_id => $group) {
		$pattern = "|\<a href='(.*?)'\>(.*?)\<\/a\>|si";
		preg_match_all($pattern, $group, $links, PREG_SET_ORDER);
		// бегаем по аномалькам
		foreach($links as $link) {
			$a_title = $link[2];
			echo "\t$a_title<br>";
			$a_html = file_get_contents($link[1]);
			// заменяем урлы картинок
			$a_html = str_replace('http://www.ellatha.com/eve/', '', $a_html);
			$a_pattern = '|\<table border="0" width="100\%" class="box"\>\s+\<tr\>\s+\<td width="100\%"\>(.*?)\<\/td\>|si';
			preg_match($a_pattern, $a_html, $anomaly);
			$a_info = $anomaly[1];
			// сохраняем
			$a = new Anomaly($a_title, $types[$type_id], $a_info);
			$a->assignToClass($class_id);
			echo "ID: ".$a->id."<br><br>";
		}
	}
}

?>
