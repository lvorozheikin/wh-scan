<?
if(!empty($_REQUEST['params']['id'])) {
	$anomaly_id = intval($_REQUEST['params']['id']);
	$anomaly = new Anomaly($anomaly_id);
	echo "<h3>{$anomaly->title}</h3>";
	echo "<div class=\"anomaly-container\">{$anomaly->description}</div>";
}
else {
	echo "<b>Anomaly not found!</b>";
}
?>