<?
// Core functions
function display($template, $params = array()) {
	if(is_array($params)) {
		// делаем элементы массива локальными переменными по ключу
		foreach($params as $name=>$val) {
			$$name = $val;
		}
	}
	// выводим
	eval('?>'.file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.$template).'<?');	
}

// ---------------------------------------------------- CHARACTER -----------------------------------------------------------
class Character {
	private $table = 'characters';
	private $db = null;
	
	public $id;
	public $title;
		
    function __construct($id = 0, $title = '') {
		global $db;
		$this->db = &$db;
		if(!$id) die('Character ID is not specified!');
		
		// пробуем добавить
		$this->id = $this->insert($id, $title);
		// получаем инфу по пользователю
		$char = $this->get($this->id);
		$this->title = $char['title'];		
	}

	public static function migrate() {
        global $db;
        $sql = "CREATE TABLE `characters` (
			`character_id` INTEGER NOT NULL PRIMARY KEY,
			`title` VARCHAR (255)
		)";
        if($stmt = $db->prepare($sql)) {
            $stmt->execute();
        }
	}
	
	public function get($id) {
		$sql = "SELECT * FROM `{$this->table}` WHERE `character_id` = ?";
		if($stmt = $this->db->prepare($sql)) {
			$stmt->execute(array($id));
			return $stmt->fetch();
		}
		else return false;
	}
	
	// получение списка чаров по имени
	public function getByName($title) {
		$sql = "SELECT * FROM `{$this->table}` WHERE `title` LIKE ?";
		if($stmt = $this->db->prepare($sql)) {
			$stmt->execute(array('%'.$title.'%'));
			return $stmt->fetchAll();
		}
		else return false;
	}
	
	public function insert($id, $title) {
		$sql = "INSERT IGNORE INTO `{$this->table}` (`character_id`, `title`) VALUES (?, ?)
				ON DUPLICATE KEY UPDATE `character_id` = LAST_INSERT_ID(`character_id`)";
		if($stmt = $this->db->prepare($sql)) {
			if($stmt->execute(array($id, $title))) {
				return $this->db->lastInsertId();
			}
			else return false;
		}
		else return false;
	}
}

// ---------------------------------------------------- CORPORATION -----------------------------------------------------------
class Corporation {
	private $table = 'corporations';
	private $db = null;
	
	public $id;
	public $title;
	public $description;
	
    function __construct($id = 0, $title = '', $description = '') {
		global $db;
		$this->db = &$db;
		if(!$id) die('Corporation ID is not specified!');
		
		// пробуем добавить
		$this->id = $this->insert($id, $title, $description);
		// получаем инфу по корпе
		$corp = $this->get($id);
		$this->title = $corp['title'];		
		$this->description = $corp['description'];		
	}

    public static function migrate() {
        global $db;
        $sql = "CREATE TABLE `corporations` (
			`corporation_id` INTEGER NOT NULL PRIMARY KEY,
			`title` VARCHAR (255),
			`description` TEXT
		)";
        if($stmt = $db->prepare($sql)) {
            $stmt->execute();
        }
    }
	
	public function get($id) {
		$sql = "SELECT * FROM `{$this->table}` WHERE `corporation_id` = ?";
		if($stmt = $this->db->prepare($sql)) {
			$stmt->execute(array($id));
			return $stmt->fetch();
		}
		else return false;
	}
	
	// получение списка корп по имени
	public function getByName($title) {
		$sql = "SELECT * FROM `{$this->table}` WHERE `title` LIKE ?";
		if($stmt = $this->db->prepare($sql)) {
			$stmt->execute(array('%'.$title.'%'));
			return $stmt->fetchAll();
		}
		else return false;
	}
	
	public function insert($id, $title, $description = '') {
		$sql = "INSERT INTO `{$this->table}` (`corporation_id`, `title`, `description`) VALUES (?, ?, ?)
				ON DUPLICATE KEY UPDATE `corporation_id` = LAST_INSERT_ID(`corporation_id`)";
		if($stmt = $this->db->prepare($sql)) {
			if($stmt->execute(array($id, $title, $description))) {
				return $this->db->lastInsertId();
			}
			else return false;
		}
		else return false;
	}
}
// ---------------------------------------------------- SYSTEM ----------------------------------------------------------------
class System {
	private $table = 'systems';
	private $db = null;
	
	public $id;
	public $title;
	public $description;
	public $security;
	public $effect_id;
	public $class_id;
			
    function __construct($id = 0, $title = '', $description = '', $security = 0, $effect_id = null, $class_id = null) {
		global $db;
		$this->db = &$db;
		if(!$id) die('System ID is not specified!');
		
		// получаем инфу по системе
		// сделать сюда кэширование
		$system = $this->getByName($title);
		
		$this->id = $system['system_id'];
		if($system) {
			// если айдишник не измененный
			if($system['system_id'] != $id) {
				// апдейтим id в базе
				$this->updateID($system['system_id'], $id);
				
				$this->id = $id;
			}
			$this->title = $system['title'];		
			$this->description = $system['description'];		
			$this->security = $system['security'];	
			$this->effect_id = $system['effect_id'];	
			$this->class_id = $system['class_id'];	
		}
		else {
			$this->title = $title;		
			$this->description = $description;	
			$this->security = $security;
			$this->effect_id = $effect_id;
			$this->class_id = $class_id;
		}
	}

    public static function migrate() {
        global $db;
        $sql = "CREATE TABLE `systems` (
			`system_id` INTEGER NOT NULL PRIMARY KEY,
			`title` VARCHAR (255),
			`description` TEXT,
			`security` FLOAT NOT NULL DEFAULT 0,
			`effect_id` INTEGER DEFAULT NULL,
			`class_id` INTEGER DEFAULT NULL
		)";
        if($stmt = $db->prepare($sql)) {
            $stmt->execute();
        }
    }
	
	public static function get($id) {
		global $db;
		$sql = "SELECT * FROM `systems` WHERE `system_id` = ?";
		if($stmt = $db->prepare($sql)) {
			$stmt->execute(array($id));
			return $stmt->fetch();
		}
		else return false;
	}
	
	// получение списка систем по имени
	public function getByName($title) {
		$sql = "SELECT * FROM `{$this->table}` WHERE `title` LIKE ?";
		if($stmt = $this->db->prepare($sql)) {
			$stmt->execute(array(trim($title)));
			return $stmt->fetch();
		}
		else return false;
	}
	
	public function insert($id, $title, $description = '', $security = 0, $effect_id = null, $class_id = null) {
		$sql = "INSERT INTO `{$this->table}` (`system_id`, `title`, `description`, `security`, `effect_id`, `class_id`) VALUES (?, ?, ?, ?, ?, ?)
				ON DUPLICATE KEY UPDATE `system_id` = LAST_INSERT_ID(`system_id`)";
		if($stmt = $this->db->prepare($sql)) {
			if($stmt->execute(array($id, $title, $description, $security, $effect_id, $class_id))) {
				return $this->db->lastInsertId();
			}
			else return false;
		}
		else return false;
	}
	
	public function getAnomalies() {
		return Anomaly::getAnomaliesByClass($this->class_id);
	}
	
	private function updateID($old_id, $new_id) {
		$sql = "UPDATE `{$this->table}` SET `system_id` = ? WHERE `system_id` = ?";
		if($stmt = $this->db->prepare($sql)) {
			return $stmt->execute(array($new_id, $old_id));
		}		
		return false;
	}
}
// ---------------------------------------------------- VISIT -----------------------------------------------------------------
class Visit {
	private $table = 'visits';
	private $db = null;
	
	public $id;
	public $character = null;
	public $corporation = null;
	public $system = null;
	public $ship = 'unknown';
	
	// добавляем
    function __construct($char, $corp, $system, $ship) {
		global $db;
		$this->db = &$db;
		
		// если 1м параметром задан id визита
		if(is_numeric($char)) {
			$this->get(intval($char));
		}
		// пробуем добавить
		else if($this->id = $this->insert($char->id, $corp->id, $system->id, $ship)) {
			$this->character = $char;
			$this->system = $system;
			$this->corporation = $corp;
			$this->ship = $ship;
		}		
	}

    public static function migrate() {
        global $db;
        $sql = "CREATE TABLE `visits` (
			`visit_id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`character_id` INTEGER DEFAULT NULL,
			`corporation_id` INTEGER DEFAULT NULL,
			`system_id` INTEGER DEFAULT NULL,
			`ship` VARCHAR (255) DEFAULT '',
			`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
		)";
        if($stmt = $db->prepare($sql)) {
            $stmt->execute();
        }
    }

	// получаем
	function get($id) {
		// пробуем получить
		if($visit = $this->get($id)) {
			$this->id = $visit['visit_id'];
			$this->character = new Character($visit['character_id']);
			$this->system = new System($visit['system_id']);
			$this->corporation = new Corporation($visit['corporation_id']);
			$this->ship = $visit['ship'];
		}			
		else {
			$this->id = null;
		}
	}
	
	private function getById($id) {
		$sql = "SELECT * FROM `{$this->table}` WHERE `visit_id` = ?";
		if($stmt = $this->db->prepare($sql)) {
			$stmt->execute(array($id));
			return $stmt->fetch();
		}
		else return false;
	}
	
	public function insert($char_id, $corp_id, $system_id, $ship) {
		$sql = "INSERT INTO `{$this->table}` (`character_id`, `corporation_id`, `system_id`, `ship`) VALUES (?, ?, ?, ?)";
		if($stmt = $this->db->prepare($sql)) {
			if($stmt->execute(array($char_id, $corp_id, $system_id, $ship))) {
				return $this->db->lastInsertId();
			}
			else return false;
		}
		else return false;
	}
}

// ---------------------------------------------------- ANOMALIES -----------------------------------------------------------------
class Anomaly {
	private $table = 'anomalies';
	private $db = null;
	
	public $id = 0;
	public $type = 'Anomaly';
	public $title = '';
	public $description = '';
	
	// добавляем
    function __construct($title, $type = 'Anomaly', $description = '') {
		global $db;
		$this->db = &$db;
		if(!empty($title)) {	
			// если первый параметр - число, то берем по ID
			if(is_numeric($title)) 
				$anomaly = $this->get(intval($title));
			else 
				$anomaly = $this->getByName($title);
			
			if(empty($anomaly)) {
				$this->id = $this->insert($title, $type, $description);
			}
			else {
				$this->id = $anomaly['anomaly_id'];
				$title = $anomaly['title'];
				$type = $anomaly['type'];
				$description = $anomaly['description'];
			}

			$this->type = $type;
			$this->title = $title;
			$this->description = $description;
		}	
	}

    public static function migrate() {
        global $db;
        $sql = "CREATE TABLE `anomalies` (
			`anomaly_id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`type` VARCHAR (255),
			`title` VARCHAR (255),
			`description` TEXT
		)";
        if($stmt = $db->prepare($sql)) {
            $stmt->execute();
        }

        $sql = "CREATE TABLE `system_class_anomalies` (
			`class_id` INTEGER NOT NULL,
			`anomaly_id` INTEGER NOT NULL
		)";
        if($stmt = $db->prepare($sql)) {
            $stmt->execute();
        }
    }
	
	private function get($id) {
		$sql = "SELECT * FROM `{$this->table}` WHERE `anomaly_id` = ?";
		if($stmt = $this->db->prepare($sql)) {
			$stmt->execute(array($id));
			return $stmt->fetch();
		}
		else return false;
	}
	
	private function getByName($title) {
		$sql = "SELECT * FROM `{$this->table}` WHERE `title` LIKE ?";
		if($stmt = $this->db->prepare($sql)) {
			$stmt->execute(array($title));
			return $stmt->fetch();
		}
		else return false;
	}
	
	public function insert($title, $type, $description) {
		$sql = "INSERT INTO `{$this->table}` (`type`, `title`, `description`) VALUES (?, ?, ?)";
		if($stmt = $this->db->prepare($sql)) {
			if($stmt->execute(array($type, $title, $description))) {
				return $this->db->lastInsertId();
			}
			else return false;
		}
		else return false;
	}
	
	public function assignToClass($class_id) {
		$sql = "INSERT IGNORE INTO `system_class_anomalies` (`class_id`, `anomaly_id`) VALUES (?, ?)";
		if($stmt = $this->db->prepare($sql)) {
			if($stmt->execute(array($class_id, $this->id))) {
				return true;
			}
		}
		return false;
	}
	
	public static function getAnomaliesByClass($class_id) {
		global $db;
		$anomalies = array();
		$sql = "SELECT * FROM `anomalies` LEFT JOIN `system_class_anomalies` USING(`anomaly_id`) WHERE `class_id` = ?";
		if($stmt = $db->prepare($sql)) {
			$stmt->execute(array($class_id));
			foreach($stmt->fetchAll() as $a) {
				$anomalies[$a['type']][$a['anomaly_id']] = $a;
			}
		}
		return $anomalies;
	}
}
?>
