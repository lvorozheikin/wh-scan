		<div class="grid5 clearfix">
			<div class="grid2">
				<h2>SYSTEM: <a href="#" class="showsystem"><?=$system->title; ?></a></h2>
			</div>
			<div class="grid3">
				<p>
					Character: <b><?=$character->title; ?></b><br />
					Corporation: <b><?=$corporation->title; ?></b><br />
					Ship: <b><?=$ship; ?></b>
				</p>
			</div>
		</div>
		<div class="grid5 clearfix">
			<div class="grid2">
			<? foreach($system->getAnomalies() as $group_type=>$a_group) : ?>
				<h4><?=$anomaly_types[$group_type]; ?></h4>
				<ul class="anomaly-list">
					<? foreach($a_group as $a_id=>$a) : ?>
						<li><a href="/index.php?snippet=anomaly&params[id]=<?=$a_id;?>"><?=$a['title']; ?></a></li>
					<? endforeach; ?>
				</ul>
			<? endforeach; ?>
			</div>
			<div class="grid3">
			
			</div>
		</div>
		
		<script>
	$(function () {
		// получае инфу по системе
		$('.showsystem').click(function () {
			CCPEVE.showInfo(5, <?=$system->id; ?>);
			return false;
		});
	});
	</script>